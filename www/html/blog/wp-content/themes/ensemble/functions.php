<?php

function ensemble_setup() {
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'custom-header', array(
  'video' => true
));

add_theme_support( 'post-formats', array(
	'video',
	'quote',
	'link',
	'gallery',
	) );

register_nav_menus(

);
}


add_action( 'after_setup_theme', 'ensemble_setup' );


function ensemble_widget_init(){
register_sidebar( array (
'name' => __( 'Sidebar', 'ensemble' ),
'id' => 'sidebar',
'before_widget' => '<div>',
'after_widget' => "</div>",
'before_title' => '<h2 class="widget-title">',
'after_title' => '</h2>',
) );

register_sidebar( array (
'name' => __( 'Sponsor Widget Area', 'ensemble' ),
'id' => 'sponsor-widget-area',
'before_widget' => '<div>',
'after_widget' => "</div>",
'before_title' => '<h2 class="sponsor-title">',
'after_title' => '</h2>',
) );

}

add_action( 'widgets_init', 'ensemble_widget_init' );

function ensemble_styles() {

wp_enqueue_style( 'ensemble-style', get_stylesheet_uri() );

}

add_action ( 'wp_enqueue_scripts', 'ensemble_styles');

function create_posttype() {

	register_post_type( 'portfolio',
		array(
			'labels' => array(
				'name' => __( 'Portfolio Items' ),
				'singular_name' => __( 'Portfolio Item' )
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'portfolio'),
			'supports' => array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				)

		)
	);
}

add_action( 'init', 'create_posttype' );

function posts_link_next_class($format){
     $format = str_replace('href=', 'class="btn btn-outline-primary float-right" href=', $format);
	$format=str_replace('&raquo;','',$format);

     return $format;
}
add_filter('next_post_link', 'posts_link_next_class');

function posts_link_prev_class($format) {
     $format = str_replace('href=', 'class="btn btn-outline-primary" href=', $format);
		$format=str_replace('&laquo;','',$format);
	

     return $format;
}
add_filter('previous_post_link', 'posts_link_prev_class');

function category_add_class($format)
{
	 $format = str_replace('<a', '<span class="badge bg-ensemble mx-1 my-1"><a', $format);
	$format=str_replace('</a>','</a></span> ',$format);

     return $format;
}
add_filter('the_category', 'category_add_class');